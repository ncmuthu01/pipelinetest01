#!/usr/bin/env bash


echo "Returning the license"

xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' \
    unity-editor \
      -batchmode \
      -nographics \
      -logFile /dev/stdout \
      -quit \
      -returnlicense
